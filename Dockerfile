FROM public.ecr.aws/docker/library/alpine:latest AS clamav_updater
RUN apk update --no-cache \
    && apk upgrade --no-cache \
    && apk add --no-cache clamav clamav-dev
COPY ./config/clamd.conf /etc/clamav/clamd.conf
COPY ./config/freshclam.conf /etc/clamav/freshclam.conf
VOLUME ["/var/log/clamav", "/var/lib/clamav"]
RUN chown clamav:clamav /var/log/clamav /var/lib/clamav
USER clamav
COPY --chown=clamav:clamav ./scripts/clamav-updater_entrypoint.sh /entrypoint
RUN chmod +x /entrypoint
ENTRYPOINT ["/entrypoint"]
LABEL maintainer="MyCareersFuture Team, GovTech Singapore"
LABEL repository_git="https://gitlab.com/mycf.sg/img-clamav"
LABEL repository_docker="https://ap-southeast-1.console.aws.amazon.com/ecr/repositories/private/936327878790/mcf/clamav?region=ap-southeast-1"

FROM clamav_updater AS clamav
COPY --chown=clamav:clamav ./scripts/clamav_entrypoint.sh /entrypoint
RUN chmod +x /entrypoint
ENTRYPOINT ["/entrypoint"]
EXPOSE 3310
