#!/bin/sh

# this is the update process
freshclam --user=root --verbose --debug;

# this lets database consumers read from it
chown clamav:clamav -R /var/lib/clamav;
