GIT_COMMIT=$$(git rev-parse --verify HEAD)
GIT_TAG=$$(git describe --tag $$(git rev-list --tags --max-count=1))
KIND_CONTEXT=mycfsg-img-clamav
IMAGE_URL_SERVICE=mycfsg/clamav
IMAGE_URL_UPDATER=mycfsg/clamav-updater
TIMESTAMP=$$(date +'%Y%m%d%H%M%S')

# use this to override any of the above properties in
# the ci pipeline as required
-include ./Makefile.properties

# starts an example deployment of the clamav service
# using docker-compose
start:
	docker-compose -f ./deploy/docker-compose.yaml up -V --build

# initialises a development kubernetes cluster
init_k8s:
	if ! kind get clusters | grep $(KIND_CONTEXT); then \
		kind create cluster \
			--config ./deploy/kind.config.yaml \
			--name $(KIND_CONTEXT); \
	fi

# starts the application on the initialised development
# kubernetes cluster
start_k8s: build
	kubectl config use-context kind-$(KIND_CONTEXT)
	kind load docker-image --name $(KIND_CONTEXT) $(IMAGE_URL_SERVICE):latest
	kind load docker-image --name $(KIND_CONTEXT) $(IMAGE_URL_UPDATER):latest
	kubectl apply -f ./deploy/kubernetes/service.yaml
	kubectl apply -f ./deploy/kubernetes/statefulset.yaml
	kubectl apply -f ./deploy/kubernetes/cronjob.yaml
	kubectl apply -f ./deploy/kubernetes/persistentvolumeclaim.yaml

# stops the application on the development cluster
stop_k8s:
	kubectl config use-context kind-$(KIND_CONTEXT)
	kubectl delete -f ./deploy/kubernetes/service.yaml
	kubectl delete -f ./deploy/kubernetes/statefulset.yaml
	kubectl delete -f ./deploy/kubernetes/cronjob.yaml
	kubectl delete -f ./deploy/kubernetes/persistentvolumeclaim.yaml

# destroys the development kubernetes cluster
denit_k8s:
	if kind get clusters | grep $(KIND_CONTEXT); then \
		kind delete cluster \
			--name $(KIND_CONTEXT); \
	fi

# builds the image
build:
	@$(MAKE) build_service \
		& $(MAKE) build_updater \
		& wait
build_service:
	docker build \
		--target clamav \
		--tag $(IMAGE_URL_SERVICE):latest \
		.
build_updater:
	docker build \
		--target clamav_updater \
		--tag $(IMAGE_URL_UPDATER):latest \
		.

# archives the image into a .tar.gz
save:
	mkdir -p ./output
	@$(MAKE) save_service \
		& $(MAKE) save_updater \
		& wait
save_service:
	docker save \
		--output ./output/service.tar.gz \
		$(IMAGE_URL_SERVICE):latest
save_updater:
	docker save \
		--output ./output/updater.tar.gz \
		$(IMAGE_URL_UPDATER):latest

# loads the image archive from .tar.gz
load:
	@$(MAKE) load_service \
		& $(MAKE) load_updater \
		& wait
load_service:
	docker load \
		--input ./output/service.tar.gz
load_updater:
	docker load \
		--input ./output/updater.tar.gz

# runs container-structure-test against the generated
# image (make sure you run `make build` first)
test:
	@$(MAKE) test_service \
		& $(MAKE) test_updater \
		& wait
test_service: build_service
	container-structure-test test \
		--config ./tests/clamav.yaml \
		--image $(IMAGE_URL_SERVICE):latest
test_updater: build_updater
	container-structure-test test \
		--config ./tests/clamav-updater.yaml \
		--image $(IMAGE_URL_UPDATER):latest

# publish the image to the docker registry
publish:
	@$(MAKE) publish_service \
		& $(MAKE) publish_updater \
		& wait
publish_service: build_service	
	docker push $(IMAGE_URL_SERVICE):latest
	docker tag $(IMAGE_URL_SERVICE):latest $(IMAGE_URL_SERVICE):$(TIMESTAMP)
	docker push $(IMAGE_URL_SERVICE):$(TIMESTAMP)
	docker tag $(IMAGE_URL_SERVICE):latest $(IMAGE_URL_SERVICE):$(GIT_COMMIT)
	docker push $(IMAGE_URL_SERVICE):$(GIT_COMMIT)
publish_updater: build_updater	
	docker push $(IMAGE_URL_UPDATER):latest
	docker tag $(IMAGE_URL_UPDATER):latest $(IMAGE_URL_UPDATER):$(TIMESTAMP)
	docker push $(IMAGE_URL_UPDATER):$(TIMESTAMP)
	docker tag $(IMAGE_URL_UPDATER):latest $(IMAGE_URL_UPDATER):$(GIT_COMMIT)
	docker push $(IMAGE_URL_UPDATER):$(GIT_COMMIT)

# runs the images in a primitive manner (you shouldn't
# need to run this except for debugging)
run:
	@$(MAKE) run_service \
		& $(MAKE) run_updater \
		& wait
run_service: build_service
	docker run \
		--volume $$(pwd)/data/lib:/var/lib/clamav \
		--volume $$(pwd)/data/log:/var/log/clamav \
		--publish 3310:3310 \
		$(IMAGE_URL_SERVICE):latest
run_updater: build_updater
	docker run \
		--volume $$(pwd)/data/lib:/var/lib/clamav \
		--volume $$(pwd)/data/log:/var/log/clamav \
		$(IMAGE_URL_UPDATER):latest
