const express = require('express');
const fs = require('fs');
const crypto = require('crypto');
const {Readable} = require('stream');
const jwt = require('jsonwebtoken');
const morgan = require('morgan');

const NodeClam = require('clamscan');

const server = express();
const ClamScan = new NodeClam().init({
  clamdscan: {
    host: 'localhost',
    port: 3310,
  },
});

server.use(morgan('combined'));
server.use(express.raw({type: '*/*', limit: process.env.MAX_FILE_SIZE || '100mb'}));

server.get('/healthz', async (_, response) => {
  response.send('ok');
});

server.get('/readyz', async (_, response) => {
  try {
    // clamav service is required for scanning
    const clamscan = await ClamScan;
    const version = await clamscan.get_version();
    if (version.code == 'ECONNREFUSED') {
      throw new Error('clam not up');
    }
  } catch (ex) {
    response.status(500);
    response.json({message: 'not yet in the sea'});
    return;
  }

  response.status(200);
  response.json({message: 'ok'});
});

server.post('/', async (request, response) => {
  const buffer = request.body;

  const readable = new Readable();
  readable.push(buffer);
  readable.push(null);

  const clamscan = await ClamScan;
  try {
    response.send(await clamscan.scan_stream(readable));
  } catch (error) {
    console.error({error});
    response.json(error).status(500);
  }
});

const port = 3000
server.listen(port, () => console.log(`listening on port ${port}`));
