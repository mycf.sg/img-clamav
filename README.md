# ClamAV Images

ClamAV Docker images for cloud-native deployments.

[![pipeline status](https://gitlab.com/mycf.sg/img-clamav/badges/master/pipeline.svg)](https://gitlab.com/mycf.sg/img-clamav/-/commits/master)

This repository contains recipes for two Docker images:

1. [`mycfsg/clamav`](https://hub.docker.com/r/mycfsg/clamav): known internally as **the service**. Starts the ClamAV service for consumption by another service to scan files for malware
1. [`mycfsg/clamav-updater`](https://hub.docker.com/r/mycfsg/clamav-updater): known internally as **the updater**. Starts the `freshclam` process to update the virus definitions.


- - -


# Assumed Architecture

[![architecture diagram](docs/diagram.png)](docs/diagram.png)


- - -


# Deployment

## Docker

Docker is used primarily for provisioning a development environment and is used to simplify image modification. To see it's likely behaviour in production, it's recommended to deploy the development Kubernetes cluster (instructions below).

> For more details, check out [`./deploy/docker-compose.yaml`](./deploy/docker-compose.yaml).

### Setting up local Docker-Compose

> Confirm that the `docker-compose` command is available (run `docker-compose version`)

1. Run `make start`

## Kubernetes

These images were prepared for deployment onto a Kubernetes cluster with cloud-native principles in mind. Although the feedback cycle is relatively longer and the development workflow significantly more troublesome, it's recommended to test out the behaviour of the images using a development Kubernetes cluster before usage.

> For more details, check out [the contents of `./deploy/kubernetes`](./deploy/kubernetes).

### Setting up local Kubernetes-in-Docker

> Confirm that the `kind` binary is available (run `kind version`).

1. Run `make init_k8s` to create the development cluster.
2. Run `make start_k8s` to deploy the application on the cluster.
3. To stop the application on the cluster, run `make stop_k8s`. This doesn't bring down the cluster. Consider this a *restart* instead of a reboot.
4. When done/if you want to tear the whole cluster down, run `make denit_k8s` to bring down the development cluster.

### Notes on developing in Kubernetes

1. The `CronJob` type is difficult to work with, so there's a `Job` manifest that isn't applied using `make start_k8s`. You can apply it manually using `kubectl apply -f ./deploy/kubernetes/job.yaml`

### Deploying in a production Kubernetes cluster

All manifests are available in [`./deploy/kubernetes`], extract as needed.


- - -


# Contributing

## Flow

1. Raise an issue on the work you're doing
2. Fork this repository
3. Make your changes in your own fork
4. Request for a review from one of the repository owners/maintainers
5. Approval will come

## Basic setup stuff

1. A `Makefile` is used to document development workflows, remember to include useful scripts you've run in it before pushing
2. You'll need [`docker`](https://docs.docker.com/get-docker/) to build the image
3. You'll need [`docker-compose`](https://docs.docker.com/compose/install/) to run the Docker Compose orchestration
5. Development using a Kubernetes cluster is recommended and done using [`kubernetes-in-docker (kind)`](https://github.com/kubernetes-sigs/kind/)
4. Testing is done using [`container-structure-test`](https://github.com/GoogleContainerTools/container-structure-test)


- - -


# License

Code is licensed under [the MIT license](./LICENSE) under GovTech Singapore.
